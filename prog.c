#include <cstdio>
#include <cmath>
#include <cstdlib>

long long int pot (long long int a, long long int b, long long int m)
{
    long long int w = 1;
    long long int x = a%m;

    for (int i = 1; i <= b; i<<=1)
    {
        x %= m;
        if((b&i) != 0)
        {
            w *= x;
            w %= m;
        }
        x *= x;
    }
    return w;
}
int main()
{
    int n;
    long long int a,b,m,w;
    scanf("%d", &n);
    for(int i=0;i<n;i++)
    {
    scanf("%lld%lld%lld", &a, &b, &m);
    w = pot(a,b,m);
    printf("%lld\n", w);
    }
    return 0;
}
