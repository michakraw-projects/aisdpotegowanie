**Zadanie
**

Napisz program, który dla danych liczb naturalnych a, b, m oblicza ab mod m. Możesz założyć, że wszystkie te liczby są nie większe od 109.

**
Wejście**

W pierwszym wierszu znajduje się liczba naturalna t, nie większa niż 10000. 
W (i+1)-szym wierszu (i=1,2,…,t) znajdują się liczby ai, bi, mi – jak wyspecyfikowano wyżej.

**
Wyjście**

W i-tym wierszu (i=1,...,t) należy wypisać wartość ai bi mod mi.

**Przykład**

Dla danych wejściowych

2

9 199 10

95925179 427342114 1000000000


poprawną odpowiedzią jest

9

272569881